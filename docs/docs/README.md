---
home: true
#heroImage: /hero.png
actionText: 快速上手 →
actionLink: /guide/
features:
- title: 简洁至上
  details: 以 Lerna 为中心构建的项目结构，简洁方便易扩展。
- title: 开箱即用
  details: 可轻松嵌入到任何任何 Node.js 框架中,比如 Express、Nest、Egg、Koa、Sails、Meteor 等。
- title: 微信公众号
  details: 支持回调配置、素材管理、自定义菜单、消息推送、客服消息、网页授权登录、模板消息、订阅消息等常用的接口。
- title: 微信小程序
  details: 支持登录、客服消息、订阅消息、小程序码、OCR、图像处理等常用的接口。
- title: 微信小游戏
  details: 支持登录、订阅消息、排行榜、小程序码等常用的接口
- title: 企业微信
  details: 支持回调配置、自定义菜单、消息推送、通讯录管理、应用管理、素材管理、OA数据管理等常用的接口
- title: 微信支付
  details: 付款码支付、公众号支付、扫码支付、APP支付、H5支付、小程序支付等常用的支付方式。
- title: AccessToken
  details: 自动管理 AccessToken，本地缓存、Redis、MongoDB 只要你玩得溜那都是支持的。  
- title: HTTP请求库易扩展
  details: Axios、Fetch、Request 等，随心所欲，想用啥就用啥。
footer: Apache License 2.0 | Copyright © 2019-present Javen
---

<p align="center">
     <a target="_blank" href="https://gitee.com/javen205/TNWX">
     	<img src="https://gitee.com/javen205/TNWX/badge/star.svg?theme=white" ></img>
     </a>
     <a target="_blank" href="https://github.com/Javen205/TNWX">
        <img src="https://img.shields.io/github/stars/Javen205/TNWX.svg?style=social&label=Stars" ></img>
     </a>
     <a href="https://www.npmjs.com/package/tnwx" target="_blank"><img src="https://img.shields.io/npm/v/tnwx.svg" alt="NPM Version" /></a>
     <a target="_blank" href="https://github.com/Javen205/donate">
        <img src="https://img.shields.io/badge/Donate-WeChat-%23ff3f59.svg" ></img>
     </a> 
</p>

### 安装

**二者任选其一**

```bash
$ npm i tnwx 
$ yarn add tnwx
```

### 全民云计算

[阿里云主机低至2折](https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=b1hkzv2x)

[腾讯云服务器首年 88](https://cloud.tencent.com/act/cps/redirect?redirect=1048&cps_key=a21676d22e4b11a883893d54e158c1d3&from=console)

[华为云购买享受红利](https://activity.huaweicloud.com/discount_area_v5/index.html?&fromuser=aHcxMTc2NTU3MQ==&utm_source=aHcxMTc2NTU3MQ==&utm_medium=cps&utm_campaign=201905)

